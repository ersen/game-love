package com.comeon.assignment.gamelove;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.comeon.assignment.gamelove.dto.response.TopLovedGamesReponse;
import com.comeon.assignment.gamelove.resource.GameLoveResource;
import com.comeon.assignment.gamelove.service.GameLoveService;

@RunWith(MockitoJUnitRunner.class)
public class GameLoveResourceUnitTests {

	@Mock
	private GameLoveService gameLoveService;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(new GameLoveResource(gameLoveService)).build();
	}

	@Test
	public void listTopLoveds_Listed_ShouldReturnResponseStatusOK() throws Exception {
		int limit = 1;
		long gameId = 1L;
		long count = 3L;

		List<TopLovedGamesReponse> list = new ArrayList<TopLovedGamesReponse>();
		list.add(new TopLovedGamesReponse(gameId, count));

		when(gameLoveService.listTopLovedGames(limit)).thenReturn(list);
		mockMvc.perform(get("/loves/top/{limit}", limit).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(list.size())))
		.andExpect(jsonPath("$.[0].gameId").value(gameId))
		.andExpect(jsonPath("$.[0].count").value(count));
	}
}
