package com.comeon.assignment.gamelove;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.comeon.assignment.gamelove.domain.Game;
import com.comeon.assignment.gamelove.domain.Love;
import com.comeon.assignment.gamelove.domain.Player;
import com.comeon.assignment.gamelove.exception.PlayerNotFoundException;
import com.comeon.assignment.gamelove.repository.PlayerRepository;
import com.comeon.assignment.gamelove.service.PlayerService;

@RunWith(MockitoJUnitRunner.class)
public class PlayerServiceUnitTests {

	private PlayerService playerService;

	@Mock
	private PlayerRepository playerRepository;

	@Before
	public void setup() {
		playerService = new PlayerService(playerRepository);
	}

	@Test(expected = PlayerNotFoundException.class)
	public void listLovedGames_WithUnkownPlayer_ShouldThrowException() {
		when(playerRepository.findOne(1L)).thenReturn(null);
		playerService.listLovedGames(1L);
	}

	@Test
	public void listLovedGames_listed_ShouldReturnLovedGamesByPlayer() {
		Player player = new Player();
		player.setId(1L);

		when(playerRepository.findOne(1L)).thenReturn(player);

		Game g1 = new Game();
		g1.setId(1L);
		Game g2 = new Game();
		g2.setId(2L);

		Love l1 = new Love();
		l1.setPlayer(player);
		l1.setGame(g1);
		player.getLoves().add(l1);

		Love l2 = new Love();
		l2.setPlayer(player);
		l2.setGame(g2);
		player.getLoves().add(l2);

		List<Long> result = playerService.listLovedGames(1L);

		assertTrue(result.size() == player.getLoves().size());
		assertTrue(result.get(0) == player.getLoves().get(0).getGame().getId());
		assertTrue(result.get(1) == player.getLoves().get(1).getGame().getId());
	}
}
