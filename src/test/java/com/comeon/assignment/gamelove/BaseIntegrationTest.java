package com.comeon.assignment.gamelove;

import java.io.IOException;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import com.comeon.assignment.gamelove.repository.GameLoveRepository;
import com.comeon.assignment.gamelove.repository.GameRepository;
import com.comeon.assignment.gamelove.repository.PlayerRepository;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(
		webEnvironment = WebEnvironment.RANDOM_PORT,
		classes = GameLoveApplication.class)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-fast.yml")
@Transactional
public abstract class BaseIntegrationTest {

	@Autowired
	protected MockMvc mvc;

	@Autowired
	protected GameLoveRepository gameLoveRepository; 

	@Autowired
	protected GameRepository gameRepository; 

	@Autowired
	protected PlayerRepository playerRepository; 


	protected static byte[] convertObjectToJsonBytes(Object object) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		return mapper.writeValueAsBytes(object);
	}
}
