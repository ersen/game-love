package com.comeon.assignment.gamelove;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.comeon.assignment.gamelove.resource.PlayerResource;
import com.comeon.assignment.gamelove.service.PlayerService;

@RunWith(MockitoJUnitRunner.class)
public class PlayerResourceUnitTest {

	@Mock
	private PlayerService playerService;

	private MockMvc mockMvc;

	@Before
	public void setUp() {
		this.mockMvc = MockMvcBuilders.standaloneSetup(new PlayerResource(playerService)).build();
	}

	@Test
	public void listLovedGames_Listed_ShouldReturnResponseStatusOK() throws Exception {
		List<Long> loveds = new ArrayList<Long>();
		loveds.add(1L);
		loveds.add(2L);

		long playerID = 1L;
		when(playerService.listLovedGames(playerID)).thenReturn(loveds);

		mockMvc.perform(get("/players/{playerId}/loves", playerID).contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(loveds.size())))
		.andExpect(jsonPath("$.[0]").value(loveds.get(0)))
		.andExpect(jsonPath("$.[1]").value(loveds.get(1)));

	}

}
