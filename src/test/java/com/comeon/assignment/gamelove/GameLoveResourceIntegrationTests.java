package com.comeon.assignment.gamelove;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import org.springframework.http.MediaType;

import com.comeon.assignment.gamelove.domain.Game;
import com.comeon.assignment.gamelove.domain.Love;
import com.comeon.assignment.gamelove.domain.Player;
import com.comeon.assignment.gamelove.dto.request.GameLoveRequest;

public class GameLoveResourceIntegrationTests extends BaseIntegrationTest {

	private static final long GAME_ID = 1L;
	private static final long PLAYER_ID = 1L;

	@Test
	public void love_Loved_NewLoveRecordShouldBeCreatedAssociatedWithPlayerAndGame() throws Exception {
		createTestDatas();

		GameLoveRequest req = new GameLoveRequest();
		req.setGameId(GAME_ID);
		req.setPlayerId(PLAYER_ID);

		mvc.perform(post("/loves/")
			.contentType(MediaType.APPLICATION_JSON).content(convertObjectToJsonBytes(req)))
			.andExpect(status().isOk());

		Love love = gameLoveRepository.findByPlayerIdAndGameId(req.getPlayerId(), req.getGameId()).get();

		assertNotNull(love);
		assertTrue(love.getGame().getId() == req.getGameId());
		assertTrue(love.getPlayer().getId() == req.getPlayerId());
	}

	@Test
	public void love_Unloved_NoLoveRecordShouldExistsAssociatedWithPlayerAndGame() throws Exception {
		createTestDatas();

		Game game = gameRepository.findOne(GAME_ID);
		Player player = playerRepository.findOne(PLAYER_ID);

		Love newLove = new Love();
		newLove.setGame(game);
		newLove.setPlayer(player);
		gameLoveRepository.save(newLove);

		GameLoveRequest req = new GameLoveRequest();
		req.setGameId(GAME_ID);
		req.setPlayerId(PLAYER_ID);

		mvc.perform(delete("/loves/")
			.contentType(MediaType.APPLICATION_JSON).content(convertObjectToJsonBytes(req)))
			.andExpect(status().isOk());

		Optional<Love> love = gameLoveRepository.findByPlayerIdAndGameId(req.getPlayerId(), req.getGameId());
		assertFalse(love.isPresent());
	}

	private void createTestDatas() {
		Game game = new Game();
		game.setId(GAME_ID);
		gameRepository.save(game);

		Player player = new Player();
		player.setId(PLAYER_ID);
		playerRepository.save(player);
	}

}
