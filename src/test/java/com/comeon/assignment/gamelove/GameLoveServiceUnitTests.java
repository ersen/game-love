package com.comeon.assignment.gamelove;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.comeon.assignment.gamelove.domain.Game;
import com.comeon.assignment.gamelove.domain.Love;
import com.comeon.assignment.gamelove.domain.Player;
import com.comeon.assignment.gamelove.dto.response.TopLovedGamesReponse;
import com.comeon.assignment.gamelove.exception.GameNotFoundException;
import com.comeon.assignment.gamelove.repository.GameLoveRepository;
import com.comeon.assignment.gamelove.repository.GameRepository;
import com.comeon.assignment.gamelove.repository.PlayerRepository;
import com.comeon.assignment.gamelove.service.GameLoveService;

@RunWith(MockitoJUnitRunner.class)
public class GameLoveServiceUnitTests {

	private GameLoveService gameLoveService;

	@Mock
	private GameLoveRepository gameLoveRepository;

	@Mock
	private GameRepository gameRepository;

	@Mock
	private PlayerRepository playerRepository;

	@Before
	public void setup() {
		gameLoveService = new GameLoveService(gameLoveRepository, gameRepository, playerRepository);
	}

	@Test(expected = GameNotFoundException.class)
	public void create_WithInvalidGameId_ShouldThrowException() {
		when(gameRepository.findOne(1L)).thenReturn(null);
		gameLoveService.create(1L, 1L);
	}

	@Test(expected = GameNotFoundException.class)
	public void create_WithInvalidPlayerId_ShouldThrowException() {
		when(playerRepository.findOne(1L)).thenReturn(null);
		gameLoveService.create(1L, 1L);
	}

	@Test
	public void listTopLovedGames_list_ShouldReturnLimitedList() {

		Player p1 = new Player();
		p1.setId(1L);
		Player p2 = new Player();
		p2.setId(2L);
		Player p3 = new Player();
		p3.setId(3L);
		Player p4 = new Player();
		p4.setId(4L);

		Game g1 = new Game();
		g1.setId(1L);
		Game g2 = new Game();
		g2.setId(2L);
		Game g3 = new Game();
		g3.setId(3L);
		Game g4 = new Game();
		g4.setId(4L);

		List<Love> loves = new ArrayList<Love>();

		Love l1 = new Love();
		l1.setPlayer(p1);
		l1.setGame(g1);
		loves.add(l1);

		Love l2 = new Love();
		l2.setPlayer(p2);
		l2.setGame(g1);
		loves.add(l2);

		Love l3 = new Love();
		l3.setPlayer(p3);
		l3.setGame(g1);
		loves.add(l3);

		Love l4 = new Love();
		l4.setPlayer(p4);
		l4.setGame(g1);
		loves.add(l4);

		Love l5 = new Love();
		l5.setPlayer(p1);
		l5.setGame(g2);
		loves.add(l5);

		Love l6 = new Love();
		l6.setPlayer(p2);
		l6.setGame(g2);
		loves.add(l6);

		Love l7 = new Love();
		l7.setPlayer(p3);
		l7.setGame(g2);
		loves.add(l7);

		Love l8 = new Love();
		l8.setPlayer(p1);
		l8.setGame(g3);
		loves.add(l8);

		Love l9 = new Love();
		l9.setPlayer(p2);
		l9.setGame(g3);
		loves.add(l9);

		Love l10 = new Love();
		l10.setPlayer(p1);
		l10.setGame(g4);
		loves.add(l10);

		when(gameLoveRepository.findAll()).thenReturn(loves);

		List<TopLovedGamesReponse> response = gameLoveService.listTopLovedGames(2);
		assertTrue(response.size() == 2);

		assertTrue(response.get(0).getGameId() == 1);
		assertTrue(response.get(0).getCount() == 4);

		assertTrue(response.get(1).getGameId() == 2);
		assertTrue(response.get(1).getCount() == 3);
	}

}
