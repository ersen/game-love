package com.comeon.assignment.gamelove.dto.response;

public class TopLovedGamesReponse {

	private long gameId;
	private long count;

	public TopLovedGamesReponse(long gameId, long count) {
		this.gameId = gameId;
		this.count = count;
	}
	public long getGameId() {
		return gameId;
	}
	public void setGameId(long gameId) {
		this.gameId = gameId;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}

}
