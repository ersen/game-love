package com.comeon.assignment.gamelove.dto.request;

public class GameLoveRequest {

	private long gameId;
	private long playerId;

	public long getGameId() {
		return gameId;
	}
	public void setGameId(long gameId) {
		this.gameId = gameId;
	}
	public long getPlayerId() {
		return playerId;
	}
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

}
