package com.comeon.assignment.gamelove.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.comeon.assignment.gamelove.service.PlayerService;

@RestController
@RequestMapping(value = "players")
public class PlayerResource {

	PlayerService playerService;

	@Autowired
	public PlayerResource(PlayerService playerService) {
		this.playerService = playerService;
	}

	@RequestMapping(value = "/{playerId}/loves", method = RequestMethod.GET)
	public List<Long> listLovedGames(@PathVariable("playerId") long playerId) {
		return playerService.listLovedGames(playerId);
	}
}
