package com.comeon.assignment.gamelove.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.comeon.assignment.gamelove.dto.request.GameLoveRequest;
import com.comeon.assignment.gamelove.dto.response.TopLovedGamesReponse;
import com.comeon.assignment.gamelove.service.GameLoveService;


@RestController
@RequestMapping(value = "/loves")
public class GameLoveResource {

	private GameLoveService gameLoveService;

	@Autowired
	public GameLoveResource (GameLoveService gameLoveService) {
		this.gameLoveService = gameLoveService;
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public void love(@RequestBody GameLoveRequest request) {
		gameLoveService.create(request.getPlayerId(), request.getGameId());
	}

	@RequestMapping(value = "/", method = RequestMethod.DELETE)
	public void unlove(@RequestBody GameLoveRequest request) {
		gameLoveService.delete(request.getPlayerId(), request.getGameId());
	}

	@RequestMapping(value = "/top/{limit}", method = RequestMethod.GET)
	public List<TopLovedGamesReponse> listTopLoveds(@PathVariable("limit") int limit) {
		return gameLoveService.listTopLovedGames(limit);
	}

}
