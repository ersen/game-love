package com.comeon.assignment.gamelove.enums;

public enum ErrorCodes {

	INTERNAL_SERVER_ERROR(1000),
	ERR_INVALID_REQUEST_PARAMETERS(1001), 
	ERR_BOARD_NOT_FOUND(1002), 
	ERR_PLAYER_NOT_FOUND(1003), 
	ERR_GAME_LOVED_ALREADY(1004),
	ERR_GAME_NOT_FOUND(1005);

	private int code;

	ErrorCodes(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}
}