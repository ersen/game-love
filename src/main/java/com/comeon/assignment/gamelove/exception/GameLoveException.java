package com.comeon.assignment.gamelove.exception;

import com.comeon.assignment.gamelove.enums.ErrorCodes;

/**
 * Base custom exception specified for application.<br>
 * All custom exceptions must extend this class to define their special error code
 * 
 * @author ersen
 *
 */
public class GameLoveException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private int code;
	private String exceptionMessage;

	public GameLoveException(ErrorCodes code) {
		this.code = code.getCode();
	}

	public GameLoveException(ErrorCodes code, String message) {
		this(code);
		this.exceptionMessage = message;
	}

	public int getCode() {
		return code;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}
}
