package com.comeon.assignment.gamelove.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.comeon.assignment.gamelove.enums.ErrorCodes;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "No such player")
public class PlayerNotFoundException extends GameLoveException {

	private static final long serialVersionUID = 1L;

	public PlayerNotFoundException() {
		super(ErrorCodes.ERR_PLAYER_NOT_FOUND);
	}

}
