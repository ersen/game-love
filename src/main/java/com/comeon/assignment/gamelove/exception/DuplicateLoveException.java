package com.comeon.assignment.gamelove.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.comeon.assignment.gamelove.enums.ErrorCodes;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "already loved")
public class DuplicateLoveException extends GameLoveException {

	private static final long serialVersionUID = 1L;

	public DuplicateLoveException() {
		super(ErrorCodes.ERR_GAME_LOVED_ALREADY);
	}

}
