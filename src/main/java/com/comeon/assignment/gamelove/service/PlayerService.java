package com.comeon.assignment.gamelove.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.comeon.assignment.gamelove.domain.Player;
import com.comeon.assignment.gamelove.exception.PlayerNotFoundException;
import com.comeon.assignment.gamelove.repository.PlayerRepository;

@Service
public class PlayerService {

	private PlayerRepository playerRepository;

	@Autowired
	public PlayerService(PlayerRepository playerRepository) {
		this.playerRepository = playerRepository;
	}

	/**
	 * lists all game ids which player has loved
	 * 
	 * @param playerId - specified player id
	 * @throws PlayerNotFoundException in case of player not found
	 * @return game ids which specified player has loved
	 */
	public List<Long> listLovedGames(long playerId) {
		Player player = playerRepository.findOne(playerId);

		if (player == null) {
			throw new PlayerNotFoundException();
		}

		return player.getLoves().stream().map(love -> love.getGame().getId()).collect(Collectors.toList());
	}
}
