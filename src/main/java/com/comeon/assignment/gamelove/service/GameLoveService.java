package com.comeon.assignment.gamelove.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.comeon.assignment.gamelove.domain.Game;
import com.comeon.assignment.gamelove.domain.Love;
import com.comeon.assignment.gamelove.domain.Player;
import com.comeon.assignment.gamelove.dto.response.TopLovedGamesReponse;
import com.comeon.assignment.gamelove.exception.DuplicateLoveException;
import com.comeon.assignment.gamelove.exception.GameNotFoundException;
import com.comeon.assignment.gamelove.exception.PlayerNotFoundException;
import com.comeon.assignment.gamelove.repository.GameLoveRepository;
import com.comeon.assignment.gamelove.repository.GameRepository;
import com.comeon.assignment.gamelove.repository.PlayerRepository;

@Service
public class GameLoveService {

	GameLoveRepository gameLoveRepository;
	GameRepository gameRepository;
	PlayerRepository playerRepository;

	@Autowired
	public GameLoveService(GameLoveRepository gameLoveRepository, GameRepository gameRepository, PlayerRepository playerRepository) {
		this.gameLoveRepository = gameLoveRepository;
		this.gameRepository = gameRepository;
		this.playerRepository = playerRepository;
	}

	/**
	 * creates a new love record for specified playerId and gamedId 
	 * 
	 * @param playerId
	 * @param gameId
	 * 
	 * @throws GameNotFoundException in case of game not found <br />
	 * 		   {@link PlayerNotFoundException} in case of player not found <br />
	 * 		   {@link DuplicateLoveException} in case of player loved the specified game already	
	 */
	@Transactional
	public void create(long playerId, long gameId) {
		Game game = gameRepository.findOne(gameId);
		if (game == null) {
			throw new GameNotFoundException();
		}

		Player player = playerRepository.findOne(playerId);
		if (player == null) {
			throw new PlayerNotFoundException();
		}

		Love love = new Love();
		love.setGame(game);
		love.setPlayer(player);

		try
		{
			gameLoveRepository.save(love);
		}
		catch (DataIntegrityViolationException e) {
			throw new DuplicateLoveException();
		}
	}

	/**
	 * removes an existing love record for specified playerId and gamedId 
	 * 
	 * @param playerId
	 * @param gameId
	 * 
	 */
	@Transactional
	public void delete(long playerId, long gameId) {
		Optional<Love> love = gameLoveRepository.findByPlayerIdAndGameId(playerId, gameId);
		if (love.isPresent()) {
			gameLoveRepository.delete(love.get());
		}
	}

	/**
	 * Lists the most loved games according to specified limit
	 * 
	 * @param limit specifies how many games should be listed
	 * @return {@link TopLovedGamesReponse}
	 */
	public List<TopLovedGamesReponse> listTopLovedGames(int limit) {
		List<Love> loves = new ArrayList<Love>();
		gameLoveRepository.findAll().forEach(g -> loves.add(g));

		Map<Game, Long> counting = loves.stream().collect(Collectors.groupingBy(Love::getGame, Collectors.counting()));

		return counting.entrySet().stream()
				.sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
				.limit(limit)
				.map(m -> new TopLovedGamesReponse(m.getKey().getId(), m.getValue()))
				.collect(Collectors.toList());
	}

}
