package com.comeon.assignment.gamelove.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.comeon.assignment.gamelove.domain.Player;

public interface PlayerRepository extends PagingAndSortingRepository<Player, Long>{

}
