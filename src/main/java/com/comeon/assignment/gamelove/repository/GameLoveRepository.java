package com.comeon.assignment.gamelove.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.comeon.assignment.gamelove.domain.Love;

public interface GameLoveRepository extends PagingAndSortingRepository<Love, Long>{

	Optional<Love> findByPlayerIdAndGameId(Long playerId, Long gameId);

	List<Love> findByPlayerId(Long playerId);
}
