package com.comeon.assignment.gamelove.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import com.comeon.assignment.gamelove.domain.Game;

public interface GameRepository extends PagingAndSortingRepository<Game, Long>{

}
