INSERT INTO games (id, name) VALUES (1, 'Street Fighter');
INSERT INTO games (id, name) VALUES (2, 'Captain Comando');
INSERT INTO games (id, name) VALUES (3, 'Cadillacs and Dinosaurs');
INSERT INTO games (id, name) VALUES (4, 'The Punisher');

INSERT INTO players (id, name) VALUES (1, 'John Doe');
INSERT INTO players (id, name) VALUES (2, 'Jane Doe');
INSERT INTO players (id, name) VALUES (3, 'John Brown');
INSERT INTO players (id, name) VALUES (4, 'Jane Brown');