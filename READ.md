# Game Love Service

## Getting Started
You can build jar package with:

```
mvn clean package
```

This will create game-love-0.0.1-SNAPSHOT.jar file.

## Runnig

Run the created jar file as below:
```
java -jar GameLove-0.0.1-SNAPSHOT.jar
```

Or, run the service directly with maven command:
```
mvn spring-boot:run
```
